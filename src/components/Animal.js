import { Component } from "react";
import image from '../assets/images/cat.jpg'
class Animal extends Component {

    render() {
        const {kind} = this.props;

        return(
            <div>
            {kind === "cat" ?
            <img src={image} alt="cat." /> :
            <p>meow not found :)</p>   
            }
            </div>
        )
    }
}
export default Animal; 